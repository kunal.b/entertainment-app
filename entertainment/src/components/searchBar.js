import React from 'react'

export default function searchBar() {
    return (
        <form className=" seachbar d-flex mr-4 ml-4">
            <input className="form-control mr-sm-2 m-1" height="100" type="search" placeholder="Search" aria-label="Search" />
            <button className="btn btn-outline-success my-2 my-sm-0 m-1" type="submit">Search</button>
        </form>
    )
}
