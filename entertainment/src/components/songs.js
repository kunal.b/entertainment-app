import React, { useState } from 'react'

export default function Songs({ data, remove }) {

    const [likes, setlikes] = useState(data.like)


    var increment = () => {
        setlikes(likes + 1);
    }


    return (
        <div className="border-0 card bg-secondary bg-white">
            <div className="card-body flex-row d-flex">
                <div className="flex-row d-flex">
                    <div className="flex-row d-flex align-items-center ">
                        <p>{likes}</p>
                        <img onClick={increment} src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASEAAACuCAMAAABOUkuQAAAApVBMVEX29vbtMj329vX2+PjuMDv39fbtMj75+vrtLDj3+vr7+vrsKjbpJTL9+Pn+9vf+8/TjJTH96uzkIS70wMT95ObeQEr1x8vofoT6293gLjnicnnzur3609bxrrLsnaLfMTzpj5TfUFjsoqbfOkTgWGDgS1Tlm57jk5jwsLXfYGfjbXTriI3kV1/lTVfhbHP5z9PpgonnaG/WJzLsHy7wl5zaYWftycyBLNGlAAAI8klEQVR4nO2daYOiuhKGgUIDIaxu4C7qGbU9x+5z+97//9NuAvaM2i4g0QQ7z8yn/pBuXqoqVUUWTVMoFAqFQqFQKBSKOmOc+yFUGxOOBzj7K2rBwV8Oh4/UePpfIiPAZDCYRmAjBqb/wK5oPHuMzIwYxu8f1AwjdwPDtrHfHg5Gf/2ipKtuHHooU6mKGQHYNnI9z/d9z3OxzUn252MA8uJRbzYPyBdR0FlPB21qTHe/dEDIa3dH0/56sVwuZ+vNW9qKfVRDlQC58XibEMcydZ3+zzFNiwTzj1Xo3vVIAG7YnW4nVHPHskyK5RASJH/3Bu37RhQGNZ/WPwGxfktzCFVp8r5zYR+nCsG8Emy/1ZtHzrdRmU5JfxCi+mgE7mAbOWfl2T8TCT66VCOj8MRvGDgcL6k8l0Z0os44xMx9DekDN+DdIqIvunlZIfbeo48hzuQpErUBtacJ2Wt+QXqTJL+oHd0f454E2OF7cOlNH+EEb0Udw/bHE3LFJn9rNB95bESZRQLcXV7zr+MH6rTcAhKB213u9blql2zIYB3TmVJihcAbFzOgHCuY+jclQuFbiTGdycqV2NHA70XmpThx9p2Tddu+PiQazgo42MGQ0ftt1UUB4ZqUeJb8nc93+Pxgec2CR0kJo8wkoqqDnBpBuCA0KSwpkZW08MXQCt40Kjlgs6mTGTVMCetjCGdOSXV0lmabwQqfeeVZkuj3o/JDUtU7sYRGBP7iDoEykS5JBOGmtNfmOB1qRZLFa/D6dz4Nk2jwLRY1tAoCUYkWoWxW5H7e5Q97iZIu+jYidbG7BdJ1spFsRkODoGyIPpJochw4mAW57xUE0vVoWiQbfRrQnlcRSG9ayyOvoLM1HpedxY4xg9Z3uxQGeP/cGaV/Q/re0StHrWoCUYnmbXmMCK0qBKE9UfpnQjM0iKsZJavgSN+VZDozIKz4OAwzGf6pp8BfVzXKbIq0Janz8VulmLrHWtDZZ58J4zGXEZeSzGcQJ9VNiELGX1kRDDmNmGIpig/cI7f6NoUwk/2Uz8XHshEnUuSNdlwpFTrA+chrWDTi4WMMMhY/4xsafufzwllrp8WaRRBOOElOZ3wJjAja3J5Ht2Z+gwV+h4vXMkgq3ohQysslKNEKgc0p8GdYW+HTmeHPSvYArz4QnZ8r1mMnRGdq4ucCu6rVwfEDrTITat78qlEUp3e+yfs8aNDg9CyMprX1pjwHFB+rwe/wNCFdD1IOFczRgIPrH1Mejd3llQx9wW0a2yPazdAnz7D6CEwa/AVWr4a35jiT8afJKvyhyEDEM118FGKTRtQl3AMHX5q61T/3relpCv0lexhi873IViPaSB2GMsygLTBUY87Z0EMIuuIyImjzzoYeARmLU8jeBTr/HI835M0W1ou1B/IH6rx1KYIG25UgfUbNMIV98jA0m2th/yjMubguGu7XQqF/hX2eNly5q7I9tDKzBSVEhsezA/s4gp2w6Z53++xBRIJSxoYGPud+4IMgLVE2BL78vQ/GfwQq9G89FBLWqlYKvYxCYryMLVdVkfqWSDWZ7cU1iKAuGeNQWMbofqi67BoNze7VQiGBtX3WH5K9xahbC1eUQHXpMb4L7FNzW+T5SEgq8FuHV4fCTOBUpmm4Dl8UJwKXMgLmtvL5cTh9kQuIYBiIFuAabJptkpXItR/AdSUsf5hEE7HbzORfgybWydh8z3F5+EMIhBX2OYA/pHYztoZd8B48u1V9A+cjISNR38q+AH8hsxGd7MEWohAayGxE0Uj0tg7NkNqIsr2zwrG7XPe+cCXqio5C2WGBuMr5HA+F9EQuFP6DtMvOzY7wML0HraSszkyWLIp2shxwezL6Gfmk9YYhxYZ7DcKtfPOZs/Ek8TEGDKX7+urMZAlCOaglT8c6+/hidSQ6WycDjaTJiphC1kRkc/osUPXMKa5QgYRXG98A/CmNRNZ8xwSSYxrLyDwe3E9JYpHToRYk40He4I6lkIjMYlnOrjoF8Ei8RNnBuaKVuAjg1kRw6ij14csUA8VLkQVI0wpSOcr5s7Bz3gw73Iib0kyn05VYIC2/gAO8cdkzt7lB+jdOTBcN5P9xqyNkaZqVpJ6kh5ufYrf7z/c0k2yHNbn1hWaytpc+29MKXdsgCw0NAMXrzIyetMjRJLPdPkRLmSl+p6HZfjp5WjRygk9//1GjUROFNPapMX7SvG9GiyHOD66QqFYtArirTtEbciro48xTFoHqYzsHAGqzW5YeKpIV9GO5k8TrAO5uS91pUxKTdAZSHYd/BzRiJw9zNav4BV9yYhj0r2+gdr/MfVTFySO06IesDKvU3Fa566OK6eNMUt/e/4q6Y7A77Di7mhnVO0KfAjjm5WqZ0KbTWdU9Qp9iu4MlD1fLyhgz6LUR1Cw9vAnY4ZSTGTnzQe5gr2RF2Q2ZeLflUIfQCNRGh+O+BEZeMbH+Y8Vy1nSSUXaf8CtMYWcAPFxUi0b16ZPdi+1/VohGZvAu15oX/jSqtLGbVpK6dS3jC5JFI7u9uc/TnM4u87DXtiIWYdkaiDs8jSwk/9bDh8xDAHXnFrt5tQSyf27mDYq3+dfroiqZwfjVyowbQNgvHoyaupUMXqlOLQKAX+JCZGsu+ff4B0DjNU6LrjayOrF8yxKfAOBBsU2yzjIvxF45D7oA6hZZkEUWocgbN8SChvObEpF1+BPSoEvY8fUSpKmTTS7Qq/XLimFkEl21ovwG7R/rZIzrEjnrH5VIf4fZxjWJnPVXs+NHW1EWrs9UH03dWbx6N6ggaJdvlD1VyZqx21p+tPXkGBpqnUsdaSZtG0ofLQsxePW9RjPnbAeUUigH8K/TpfxmIPyeUakA72S7tRmMRF8zKhlHx4fQkE2m+NUb0mWB+HC7tVxbweXAONxubS1ZMfYzS7EruJ/7k4yaZiLuDhJ5MdhJRk4ehaJURemzQJyYTRWEroHHbMo3BZ8xKS1sTajPTlgRe0yptBjZlhW7FejWWvnYJViBtnFEXi5aA2AXbcSdal8HDO+/qmC9DvpfqBoe12moWkOhUCgUCoVCoVAoFIoy/B+QeKUBGleyHAAAAABJRU5ErkJggg==" width="40" height="30" />
                    </div>
                    <div className="flex-column align-items-center">
                        <h5>{data.title}</h5>
                        <p>{data.subtitle}</p>
                    </div>
                </div>
                <div className="flex-row align-items-center d-flex justify-content-end w-100">
                    <audio controls className="w-100" src=""></audio>

                    <div className="dropdown">
                        <a className="btn btn-light dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" />

                        <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a className="dropdown-item" onClick={() => { remove(data) }} href="#">Delete</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    )
}
