import React from 'react'

export default function navbar() {
    return (
        
        <nav className="rounded navbar navbar-light">
            <div className="container-fluid justify-content-end">
                <a className="navbar-brand ">Navbar</a>
                <form className="d-flex">
                    <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>
                </form>
            </div>
        </nav>

    )
}


