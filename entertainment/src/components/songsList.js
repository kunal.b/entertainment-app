import React from 'react'
import Songs from "./songs";

export default function songsList(props) {
    return (
        <div className="songs mt-4 mb-4">
            <h3 className="ml-4 mr-4">Songs</h3>
            <div className="d-flex flex-column">
                {props.data.map((data) => {
                    return <Songs key={data.id} data={data} remove={props.remove} />
                })}
            </div>
        </div>
    )
}
