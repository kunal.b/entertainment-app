import './App.css';
import Navbar from "./components/navbar";
import SearchBar from "./components/searchBar";
import SongsList from "./components/songsList";
import React, { useState } from 'react';

function App() {

  let remove = (del) => {

    console.log("Deleted", del)
    setData(data.filter((e) => {
      return e !== del;
    }));
  }

 
  const [data, setData] = useState([
    { id: 1, like: 3, title: "Com Truise Flightwave", subtitle: 'Reed', media: '' },
    { id: 2, like: 3, title: "Claude Debussy - Clair de lune", subtitle: 'Reed', media: '' },
    { id: 3, like: 2, title: "Culture Shock Troglodyte", subtitle: 'Doug', media: '' },
    { id: 4, like: 2, title: "Tycho Montana", subtitle: 'Reed', media: '' }
  ]);

  return (

    <>
      <div className="comps">
        <Navbar />
        <SearchBar />
        <SongsList data={data} remove={remove} />
        <button type="button" className="btn btn-primary btn-lg float-right sticky-top fixed-bottom mr-4 text-center rounded-circle">+</button>

      </div>
    </>
  );
}

export default App;
